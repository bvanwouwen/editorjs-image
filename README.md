![](https://badgen.net/badge/Editor.js/v2.0/blue)

# Image Tool

Image Block for the [Editor.js](https://editorjs.io).

Clone of (https://github.com/editor-js/image) version 2.3.2. The default package is extended with an option for custom Tunes.
Check the [README.md](https://github.com/editor-js/image) for default implementation.

## Usage

Add a new Tool to the `tools` property of the Editor.js initial config.

```javascript
import ImageTool from '@editorjs/image';

// or if you inject ImageTool via standalone script
const ImageTool = window.ImageTool;
 
var editor = EditorJS({
  ...

  tools: {
    ...
    image: {
      class: ImageTool,
      config: {
        endpoints: {
          byFile: 'http://localhost:8008/uploadFile', // Your backend file uploader endpoint
          byUrl: 'http://localhost:8008/fetchUrl', // Your endpoint that provides uploading by Url
        },
        tunes: [
          {
            name: 'alignLeft',
            icon: '<svg width="15" height="15" aria-hidden="true" focusable="false" data-prefix="fas"><path></path></svg>',
            title: 'Align left'
          },
          {
            name: 'alignCenter',
            icon: '<svg width="15" height="15" aria-hidden="true" focusable="false" data-prefix="fas"><path></path></svg>',
            title: 'Align center'
          },
          {
            name: 'alignRight',
            icon: '<svg width="15" height="15" aria-hidden="true" focusable="false" data-prefix="fas"><path></path></svg>',
            title: 'Align right'
          },
        ]
      }
    }
  }

  ...
});
```

## Config Params

Image Tool supports these configuration parameters:

| Field | Type     | Description        |
| ----- | -------- | ------------------ |
| endpoints | `{byFile: string, byUrl: string}` | Endpoints for file uploading. <br> Contains 2 fields: <br> __byFile__ - for file uploading <br> __byUrl__ - for uploading by URL |
| §tunes | `[{name: string, icon: svg, title: string}]` | Array of objects containing custom tunes to extend the default list of tunes |
| field | `string` | (default: `image`) Name of uploaded image field in POST request |
| types | `string` | (default: `image/*`) Mime-types of files that can be [accepted with file selection](https://github.com/codex-team/ajax#accept-string).|
| additionalRequestData | `object` | Object with any data you want to send with uploading requests |
| additionalRequestHeaders | `object` | Object with any custom headers which will be added to request. [See example](https://github.com/codex-team/ajax/blob/e5bc2a2391a18574c88b7ecd6508c29974c3e27f/README.md#headers-object) |
| captionPlaceholder | `string` | (default: `Caption`) Placeholder for Caption input |
| buttonContent | `string` | Allows to override HTML content of «Select file» button |
| uploader | `{{uploadByFile: function, uploadByUrl: function}}` | Optional custom uploading methods. See details below. |

Note that if you don't implement your custom uploader methods, the `endpoints` param is required. 

